#!/bin/sh
clear
echo
echo
echo "Welcome to Nadeko V2."
echo
echo "If you are here because you need to migrate your bot, you are in the right place"
echo
echo "Please make sure you have made a COPY/BACKUP of your bot."
echo
echo "Once the Backup has been made, please enter option 1. then option 2."
echo "Lastly, please enter option 3. and verify everything works."
echo "Once everything is ready, please enter option 4."
echo "Option 4 will download the V3 installer for you"
echo
echo

root=$(pwd)
choice=9
	
base_url="https://gitlab.com/hokutochen/becausekwoth/-/raw/main"

script_menu="n-menu.sh"
script_prereq="n-prereq.sh"
script_install="n-download.sh"
script_run="n-run.sh"

while [ $choice -eq 9 ]; do
	
	echo "1. Install Prerequisites"
	echo "2. Download NadekoBot"
	echo "3. Run NadekoBot"
	echo "4. Download V3 installer"
	echo "5. Exit"
	echo -n "Type in the number of an option and press ENTER"
	echo ""
	read choice	

	if [[ $choice -eq 1 ]] ; then
		echo ""
		echo "Downloading the prerequisites installer script"
		rm "$root/$script_prereq" 1>/dev/null 2>&1
		wget -N "$base_url/$script_prereq" && bash "$root/$script_prereq"
		echo ""
		choice=9
	elif [[ $choice -eq 2 ]] ; then
		echo ""
		echo "Downloading the NadekoBot installer script"
		rm "$root/$script_install" 1>/dev/null 2>&1
		wget -N "$base_url/$script_install" && bash "$root/$script_install"
		echo ""
		sleep 2s
		choice=9
	elif [[ $choice -eq 3 ]] ; then
		echo ""
		echo "Downloading the NadekoBot run script"
		rm "$root/$script_run" 1>/dev/null 2>&1
		wget -N "$base_url/$script_run" && bash "$root/$script_run"
		echo ""
		sleep 2s
		bash "$root/linuxAIO.sh"
	elif [[ $choice -eq 4 ]] ; then
		echo ""
		echo "Downloading the V3 NadekoBot run script"
		wget -N https://gitlab.com/Kwoth/nadeko-bash-installer/-/raw/master/linuxAIO.sh
		echo ""
		sleep 2s
		bash "$root/linuxAIO.sh"	
	elif [[ $choice -eq 5 ]] ; then
		echo ""
		echo "Exiting..."
		cd "$root"
		exit 0
	else
		echo "Invalid choice"
		echo ""
		choice=9
	fi
done

cd "$root"
exit 0
